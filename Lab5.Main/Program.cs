﻿using System;
using System.Windows;

using PK.Container;
using Lab5.Infrastructure;

using Lab5.DisplayForm;
using Lab5.GlownyKontract;
using Lab5.GlownyImplementacja;


namespace Lab5.Main
{
    public class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            /* Uruchomienie wątku dla aplikacji okienkowej */
            ApplicationStart();

            IContainer container = Configuration.ConfigureApp();
            container.Resolve<IGlowny>().Metoda1();
            Console.ReadKey();
            container.Resolve<IGlowny>().Metoda2();
            Console.ReadKey();
            DisplayFormExample();
            Console.ReadKey();
            /*** Koniec własnego kodu ***/
            /* Zatrzymanie wątku dla aplikacji okienkowej */
            ApplicationStop();
        }

        private static void DisplayFormExample()
        {
            var model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
                {
                    var form = new Form();
                    var viewModel = new DisplayViewModel();
                    form.DataContext = viewModel;
                    form.Show();
                    return viewModel;
                }), null);
            model.Text = "Test wyświetlacza";
        }

        public static void ApplicationStart()
        {
            var thread = new System.Threading.Thread(CreateApp);
            thread.SetApartmentState(System.Threading.ApartmentState.STA);
            thread.Start();
            System.Threading.Thread.Sleep(600);
        }

        private static void CreateApp()
        {
            var app = new Application();
            app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            app.Run();
        }

        public static void ApplicationStop()
        {
            Application.Current.Dispatcher.InvokeShutdown();
        }

    }
}


﻿using System;
using PK.Container;
using System.Reflection;
using Lab5.WyswietlaczKontrakt;
using Lab5.WyswietlaczImplementacja;
using Lab5.GlownyKontract;
using Lab5.GlownyImplementacja;
using Lab5.Container;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        public static IContainer ConfigureApp()
        {
            IWyswietlacz display = new Wyswietlacz();
            IGlowny master = new Glowny(display);
            IContainer kontener = new ImplKontener();
            kontener.Register(display);
            kontener.Register(master);
            return kontener;
        }
    }
}


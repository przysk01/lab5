﻿using System;
using System.Reflection;
using Lab5.Container;
using PK.Container;
using Lab5.WyswietlaczKontrakt;
using Lab5.WyswietlaczImplementacja;
using Lab5.GlownyImplementacja;
using Lab5.GlownyKontract;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1
        public static Type Container = typeof(ImplKontener);
        #endregion
        #region P2
        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IGlowny));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Glowny));
        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyswietlacz));
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.WyswietlaczKontrakt
{
    public interface IWyswietlacz
    {
        string Napis(string tekst);
    }
}

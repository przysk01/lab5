﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab5.WyswietlaczKontrakt;
using Lab5.GlownyKontract;

namespace Lab5.GlownyImplementacja
{
    public class Glowny : IGlowny
    {
        public void Metoda1()
        {
            Console.WriteLine("metoda pierwsza");
        }
        public void Metoda2()
        {
            Console.WriteLine("metoda druga");
        }
        private IWyswietlacz lol;
        public Glowny(IWyswietlacz lol)
        {
            this.lol = lol;
        }
    }
}
